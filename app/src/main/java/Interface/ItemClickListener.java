package Interface;

import android.view.View;

/**
 * Created by YumnaAsim on 8/31/2017.
 */
public interface ItemClickListener {
    void onClick(View view,int position,boolean isLongClick);
}
