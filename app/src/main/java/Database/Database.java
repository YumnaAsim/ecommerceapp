package Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import model.Order;

/**
 * Created by YumnaAsim on 9/2/2017.
 */
public class Database extends SQLiteAssetHelper {
    private static final String DB_NAME = "ecommerceAppDB.db";
    private static final int DB_VER = 1;
    private static final String COL_1 = "productID";
    private static final String COL_2 = "productName";
    private static final String COL_3 = "quantity";
    private static final String COL_4 = "price";
    private static final String COL_5 = "discount";
    private static final String TABLE_NAME = "OrderDetail";

    public Database(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    /*This method is used to read data from table 'order details'*/
    public List<Order> getCarts()
    {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        String[] sqlSelect = {COL_1, COL_2, COL_3, COL_4, COL_5};
        String table = TABLE_NAME;
        queryBuilder.setTables(table);
        Cursor cursor = queryBuilder.query(sqLiteDatabase,sqlSelect,null,null,null,null,null);
        final List<Order> result = new ArrayList<>();
        if (cursor.moveToFirst())
        {
            /*this method will return false if cursor is empty, and
            * Move the cursor to the first row */
            do {
                result.add(new Order(cursor.getString(cursor.getColumnIndex(COL_1)),
                        cursor.getString(cursor.getColumnIndex(COL_2)),
                        cursor.getString(cursor.getColumnIndex(COL_3)),
                        cursor.getString(cursor.getColumnIndex(COL_4)),
                        cursor.getString(cursor.getColumnIndex(COL_5))));
            }while (cursor.moveToNext());
        }
        return result;
    }

    /*this method insert data in the order details table*/
    public void addToCart(Order order)
    {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        String query = String.format("INSERT INTO "+TABLE_NAME+" ("+COL_1+","+COL_2+","+COL_3+","+COL_4+","+COL_5+")"
                        +" VALUES('%s','%s','%s','%s','%s');",
                order.getProductID(),
                order.getProductName(),
                order.getQuantity(),
                order.getPrice(),
                order.getDiscount());
        sqLiteDatabase.execSQL(query);
        sqLiteDatabase.close();
    }

    /*This method deletes data from order details table*/
    public void clearCart()
    {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String query = String.format("DELETE FROM "+TABLE_NAME);
        sqLiteDatabase.execSQL(query);
    }

    //Favorites
    public void addToFavorites(String foodID)
    {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String query = String.format("INSERT INTO Favorites(FoodID) VALUES('%s');",foodID);
        sqLiteDatabase.execSQL(query);
    }

    public void removeFromFavorites(String foodID)
    {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String query = String.format("DELETE FROM Favorites WHERE FoodID='%s';",foodID);
        sqLiteDatabase.execSQL(query);
        /* execSQL(query): Execute a single SQL statement
         that is NOT a SELECT or any other SQL statement
         that returns data.*/
    }

    public boolean isFavorite(String foodID)
    {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String query = String.format("SELECT * FROM Favorites WHERE FoodID='%s';",foodID);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        /* rawQuery(): Runs the provided SQL and
        returns a Cursor over the result set.*/
        if (cursor.getCount() <= 0)
        {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
}
