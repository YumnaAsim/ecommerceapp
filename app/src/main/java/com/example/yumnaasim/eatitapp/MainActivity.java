package com.example.yumnaasim.eatitapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import Common.Common;
import io.paperdb.Paper;
import model.User;

public class MainActivity extends Activity {
    Button btnSignUp,btnSignIn;
    TextView slogan,title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        Paper.init(this);
        setContentView(R.layout.activity_main);
        slogan = (TextView) findViewById(R.id.slogan);
        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/Nabila.otf");
        slogan.setTypeface(typeface);
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(typeface);
        btnSignIn = (Button) findViewById(R.id.signin);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SignIn.class);
                startActivity(intent);
                finish();
            }
        });

        btnSignUp = (Button) findViewById(R.id.signup);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SignUp.class);
                startActivity(intent);
                finish();
            }
        });

        //check if user info is keep remember/stored, then directs to Menu page
        String user = Paper.book().read(Common.USER_KEY);
        String password = Paper.book().read(Common.PWD_KEY);
        if (user!=null && password!=null)
        {
            if (!user.isEmpty() && !password.isEmpty())
            {
                login(user,password);
            }
        }
    }

    private void login(final String phone, final String password) {
        if (Common.isConnectedToInternet(getBaseContext())) {

            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            final DatabaseReference table = firebaseDatabase.getReference("User");
            final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Please wait ..");
            progressDialog.show();
                /*addValueEventListener is used to
                keep listening to database reference it is attached to*/
            table.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                         /*check if user exits in database*/
                    if (dataSnapshot.child(phone).exists()) {
                        progressDialog.dismiss();
                        /*DataSnapshot contains data from a Database location*/
                        /*Passing a relative path to the child() method of
                         a DataSnapshot returns another DataSnapshot for
                         the location at the specified relative path*/
                        User user = dataSnapshot.child(phone).getValue(User.class);
                            //sign in
                            Intent intent = new Intent(MainActivity.this, Home.class);
                            Common.currentUser = user;
                            startActivity(intent);
                            finish();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "User not found in database", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
        else{
            Toast.makeText(getApplicationContext(), "Please check your connection!", Toast.LENGTH_SHORT).show();
            return;
        }
    }

}
