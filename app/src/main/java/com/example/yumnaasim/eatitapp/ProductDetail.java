package com.example.yumnaasim.eatitapp;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Array;
import java.util.Arrays;

import Common.Common;
import io.paperdb.Paper;
import model.Order;
import model.Product;
import Database.Database;
import model.Rating;

public class ProductDetail extends AppCompatActivity implements RatingDialogListener{
    TextView productName, productDescription,productPrice;
    ImageView productImage;
    String productId;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference product;
    DatabaseReference ratingTable;

    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton btnCart,btnRating;
    RatingBar ratingBar;
    ElegantNumberButton numberButton;
    Product currentProduct;

    String userPhoneNumber;
    String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        firebaseDatabase = FirebaseDatabase.getInstance();
        product = firebaseDatabase.getReference("Foods");
        ratingTable = firebaseDatabase.getReference("Rating");
        Paper.init(this);

        //get user phone number
        phoneNumber = Paper.book().read(Common.USER_KEY);
        Log.v("Phone Number",""+phoneNumber);

        productName = (TextView) findViewById(R.id.product_name);
        productDescription = (TextView) findViewById(R.id.product_description);
        productPrice = (TextView) findViewById(R.id.product_price);
        productImage = (ImageView) findViewById(R.id.product_image);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        btnRating = (FloatingActionButton) findViewById(R.id.btnRating);
        btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRatingDialog();
            }
        });

        btnCart = (FloatingActionButton) findViewById(R.id.btnCart);
        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order order = new Order(productId,
                        currentProduct.getName(),
                        numberButton.getNumber(),
                        currentProduct.getPrice(),
                        currentProduct.getDiscount());
                new Database(getBaseContext()).addToCart(order);
                Toast.makeText(ProductDetail.this,"Added to cart",Toast.LENGTH_SHORT).show();

            }
        });
        numberButton = (ElegantNumberButton) findViewById(R.id.number_button);

        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);

        if (getIntent()!=null)
        productId = getIntent().getStringExtra("FoodId");
        if (!productId.isEmpty())
        {
            getProductDetail(productId);
            getProductRating(productId);
        }
        
    }

    private void getProductRating(String productId) {
        Query foodRating = ratingTable.orderByChild("foodID").equalTo(productId);
        foodRating.addValueEventListener(new ValueEventListener() {
            int count=0,sum=0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    Rating rating = postSnapshot.getValue(Rating.class);
                    sum += Integer.parseInt(rating.getRatingValue());
                    count++;
                }
               if (count!=0)
               {
                   float average = sum/count;
                   ratingBar.setRating(average);
               }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void showRatingDialog() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
                .setTitle("Rate this food")
                .setDescription("Please select some stars and give your feedback")
                .setDefaultRating(1)
                .setTitleTextColor(R.color.colorPrimary)
                .setDescriptionTextColor(R.color.colorPrimary)
                .setHint("Please write your comment here ..")
                .setHintTextColor(R.color.colorAccent)
                .setCommentBackgroundColor(R.color.colorPrimaryDark)
                .setCommentTextColor(android.R.color.white)
                .setWindowAnimation(R.style.RatingDialogFadeAnim)
                .create(ProductDetail.this)
                .show();


    }

    public void getProductDetail(String productId) {
        product.child(productId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                currentProduct = dataSnapshot.getValue(Product.class);
                Picasso.with(getBaseContext()).load(currentProduct.getImage())
                        .into(productImage);
                collapsingToolbarLayout.setTitle(currentProduct.getName());
                productPrice.setText(currentProduct.getPrice());
                productName.setText(currentProduct.getName());
                productDescription.setText(currentProduct.getDescription());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onPositiveButtonClicked(int value, @NotNull String comments) {
        if (phoneNumber!=null)
        {
            userPhoneNumber = phoneNumber;
        }
        else
        {
            userPhoneNumber = Common.currentUser.getPhoneNumber();
        }

        final Rating rating = new Rating(userPhoneNumber,
                                    productId,
                                    String.valueOf(value),
                                    comments);

        /*saving user rating onto database on SUBMIT button click*/
        ratingTable.child(userPhoneNumber)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        //check if user already exists
                        if (dataSnapshot.child(userPhoneNumber).exists())
                        {
                            //remove old value
                           ratingTable.child(userPhoneNumber).removeValue();
                            //update new value
                            ratingTable.child(userPhoneNumber).setValue(rating);
                        }
                        else {
                            //update new value
                            ratingTable.child(userPhoneNumber).setValue(rating);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void onNegativeButtonClicked() {

    }
}
