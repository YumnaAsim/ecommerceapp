package com.example.yumnaasim.eatitapp;

import Common.Common;
import Database.Database;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ViewHolder.CartAdapter;
import info.hoang8f.widget.FButton;
import model.Order;
import model.Request;

public class Cart extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference requests;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FButton placeOrder;
    TextView totalPrice;

    List<Order> cart = new ArrayList<>();
    CartAdapter cartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Cart");
        setContentView(R.layout.activity_cart);
        firebaseDatabase = FirebaseDatabase.getInstance();
        requests = firebaseDatabase.getReference("Requests");

        recyclerView = (RecyclerView) findViewById(R.id.listCart);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        placeOrder = (FButton) findViewById(R.id.place_order);

        totalPrice = (TextView) findViewById(R.id.total);
        loadListFood();

        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cart.size()<=0)
                {
                    Toast.makeText(getBaseContext(),"Your cart is empty!",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    showAlertDialog();
                }
            }
        });

    }

    private void showAlertDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(Cart.this);
        alertDialog.setTitle("One more step");
        alertDialog.setMessage("Enter your address");
        final EditText editAddress = new EditText(Cart.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        editAddress.setLayoutParams(lp);
        alertDialog.setView(editAddress);
        alertDialog.setIcon(R.drawable.ic_add_shopping_cart_black_24dp);

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //make a request object
                Request request = new Request(Common.currentUser.getUsername(),
                        editAddress.getText().toString(),
                        Common.currentUser.getPhoneNumber(),
                        totalPrice.getText().toString(),
                        cart);
                // submit this request to firebase
                //we will System.currentTimeMillis() use  as key
                requests.child(String.valueOf(System.currentTimeMillis()))
                        .setValue(request);
                //deleting cart
                new Database(Cart.this).clearCart();
                Toast.makeText(getApplicationContext(),"Order placed, Thanks!",Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void loadListFood() {
        cart = new Database(this).getCarts();
        cartAdapter = new CartAdapter(cart,this);
        cartAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(cartAdapter);

        int total = 0;
        for (Order order:cart)
        {
            total += (Integer.parseInt(order.getPrice())) *
                        (Integer.parseInt(order.getQuantity()));
        }
        Locale locale = new Locale("en","US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        totalPrice.setText(fmt.format(total));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(Common.DELETE))
            deleteCart(item.getOrder());
        return true;
    }
    /*This method deletes the order one by one*/
    private void deleteCart(int position) {
        //we will remove data from List<Order> cart
        cart.remove(position);
        //now we will delete data from SQLite db

        new Database(this).clearCart();
        //now, we will update data from List<Order> to SQLite db
        for (Order item:cart)
        {
            new Database(this).addToCart(item);
        }
        loadListFood();
    }
}

