package com.example.yumnaasim.eatitapp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import Common.Common;

import io.paperdb.Paper;
import model.User;

public class SignIn extends AppCompatActivity {

    Button btnSignIn;
    EditText editPhone, editPassword;
    TextView forgotPwdTxt;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference table;
    com.rey.material.widget.CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Sign In");
        setContentView(R.layout.activity_sign_in);
        Paper.init(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        table = firebaseDatabase.getReference("User");
        editPhone = (MaterialEditText) findViewById(R.id.phone);
        editPassword = (MaterialEditText) findViewById(R.id.password);
        forgotPwdTxt = (TextView) findViewById(R.id.forgotPass);
        forgotPwdTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotPwdDialog();
            }
        });
        btnSignIn = (Button) findViewById(R.id.sign_in);
        checkBox = (com.rey.material.widget.CheckBox) findViewById(R.id.chkRemember);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editPhone.getText().toString().equals("")
                        && editPassword.getText().toString().equals("")) {
                    Toast.makeText(getBaseContext(),
                            "Invalid Input", Toast.LENGTH_SHORT).show();
                } else {
                    if (Common.isConnectedToInternet(getBaseContext())) {
                    /*check if user has enter phone number and password*/
                        //save user and password
                        if (checkBox.isChecked()) {
                            Paper.book().write(Common.USER_KEY, editPhone.getText().toString());
                            Paper.book().write(Common.PWD_KEY, editPassword.getText().toString());
                        }
                        final ProgressDialog progressDialog = new ProgressDialog(SignIn.this);
                        progressDialog.setMessage("Please wait ..");
                        progressDialog.show();
                /*addValueEventListener is used to
                keep listening to database reference it is attached to*/
                        table.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                         /*check if user exits in database*/
                                if (dataSnapshot.child(editPhone.getText().toString()).exists()) {
                                    progressDialog.dismiss();
                        /*DataSnapshot contains data from a Database location*/
                        /*Passing a relative path to the child() method of
                         a DataSnapshot returns another DataSnapshot for
                         the location at the specified relative path*/
                                    User user = dataSnapshot.child(editPhone.getText().toString()).getValue(User.class);
                                    user.setPhoneNumber(editPhone.getText().toString());
                                    if (user.getPassword().equals(editPassword.getText().toString())) {
                                        //sign in
                                        Intent intent = new Intent(SignIn.this, Home.class);
                                        Common.currentUser = user;
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Wrong Password", Toast.LENGTH_LONG).show();
                                    }

                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "User not found in database", Toast.LENGTH_LONG).show();
                                }
                            }


                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    } else {
                        Toast.makeText(getApplicationContext(), "Please check your connection!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }

        });

    }

    private void showForgotPwdDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Forgot Password");
        alertDialog.setMessage("Enter your secure code");

        LayoutInflater layoutInflator = this.getLayoutInflater();
        View forgotView = layoutInflator.inflate(R.layout.forgot_password,null);

        alertDialog.setView(forgotView);
        alertDialog.setIcon(R.drawable.ic_security_black_24dp);

        final MaterialEditText edtPhone = (MaterialEditText) forgotView.findViewById(R.id.phone);
        final MaterialEditText edtSecureCode = (MaterialEditText) forgotView.findViewById(R.id.secureCode);

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               table.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //check if user already exists
                        User user = dataSnapshot.child(edtPhone.getText().toString())
                                .getValue(User.class);
                        if (user.getSecureCode().equals(edtSecureCode.getText().toString()))
                        {
                            Toast.makeText(getBaseContext(),"Your Password : "+user.getPassword()
                                            ,Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(getBaseContext(),"Wrong secure code !"
                                    ,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alertDialog.show();


    }


}

