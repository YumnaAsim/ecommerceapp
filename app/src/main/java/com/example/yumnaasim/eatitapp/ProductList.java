package com.example.yumnaasim.eatitapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

import Common.Common;
import Database.Database;
import Interface.ItemClickListener;
import ViewHolder.ProductViewHolder;
import model.Product;

public class ProductList extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference product;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    String categoryId;
    FirebaseRecyclerAdapter<Product, ProductViewHolder> firebaseRecyclerAdapter;

    //search funtionality
    FirebaseRecyclerAdapter<Product, ProductViewHolder> searchAdapter;
    List<String> suggestList = new ArrayList<>();
    MaterialSearchBar searchBar;

    Database localDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        this.setTitle(getIntent().getStringExtra("Category Name"));
        localDB  = new Database(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyler_view);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        if (getIntent() != null)
           categoryId = getIntent().getStringExtra("CategoryId");

            if (!categoryId.isEmpty() && categoryId != null)
            {
                if (Common.isConnectedToInternet(getBaseContext()))
                loadProduct(categoryId);
                else{
                    Toast.makeText(getApplicationContext(), "Please check your connection!", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        searchBar = (MaterialSearchBar) findViewById(R.id.search_bar);
        searchBar.setHint("Enter your food");
        loadSuggest(); // load suggestion from firebase for a particular menu ID
        searchBar.setLastSuggestions(suggestList);
        searchBar.setCardViewElevation(10);
        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //when user type their text, we will change the suggest list
                List<String> suggest = new ArrayList<String>();
                for (String search : suggestList) // enhanced for loop to traverse an array
                {
                    if (search.toLowerCase().contains(searchBar.getText().toLowerCase()))
                    {
                        suggest.add(search);
                    }
                }
                searchBar.setLastSuggestions(suggest);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        searchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener()
        {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                /*when search bar is closed restore original adapter*/
                if (!enabled)
                {
                    recyclerView.setAdapter(firebaseRecyclerAdapter);
                }
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                /*when search finish, show result of search adapter*/
                startSearch(text);

            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });
    }

    private void startSearch(CharSequence text) {
        searchAdapter = new FirebaseRecyclerAdapter<Product, ProductViewHolder>
                (Product.class,
                R.layout.product_item,
                ProductViewHolder.class,
                product.orderByChild("Name")
                        .equalTo(text.toString())

        ) {
            @Override
            protected void populateViewHolder(ProductViewHolder viewHolder, Product model, int position) {
                viewHolder.productName.setText(model.getName());
                Picasso.with(ProductList.this)
                        .load(model.getImage()).into(viewHolder.productImage);
                final Product product = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent intent = new Intent(ProductList.this,ProductDetail.class);
                        intent.putExtra("FoodId",searchAdapter.getRef(position).getKey());
                        startActivity(intent);
                    }
                });
            }
        };
        /*set adapter for recyler view i.e search result*/
        recyclerView.setAdapter(searchAdapter);
    }
    /*This method adds product name to the searchList array*/
    private void loadSuggest() {
        //we want to read data from node "Food", we've already initialized database reference
        // a listener(addValueEventListener) is attached with the node, to read data(child node) from it
        //we've given the order of reading child node
        // i.e read only that node whose MenuId=categoryId,
        product.orderByChild("MenuId").equalTo(categoryId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        /*onDataChange returns a snapshot containing all data at that location*/
                        for (DataSnapshot postSnapshot:dataSnapshot.getChildren())
                        {
                            //dataSnapshot.getChildren() : Gives access to all of the immediate children
                            // of this snapshot.
                            Product item = postSnapshot.getValue(Product.class);
//                           /*getValue() on a snapshot returns a
//                          language-specific object(i.e model) representation of the data*/
                            suggestList.add(item.getName());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    public void loadProduct(String categoryId)
    {
        firebaseDatabase = FirebaseDatabase.getInstance();
        product = firebaseDatabase.getReference("Foods");
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Product, ProductViewHolder>
                (Product.class,
                        R.layout.product_item,
                        ProductViewHolder.class,
                        product.orderByChild("MenuId")
                                .equalTo(categoryId)) {
            @Override
            protected void populateViewHolder(final ProductViewHolder viewHolder, final Product model, final int position) {
                viewHolder.productName.setText(model.getName());
                Picasso.with(ProductList.this)
                        .load(model.getImage()).into(viewHolder.productImage);

                if (localDB.isFavorite(firebaseRecyclerAdapter.getRef(position).getKey()))
                {
                    viewHolder.favoriteIcon.setImageResource(R.drawable.ic_favorite_black_24dp);
                }
                viewHolder.favoriteIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!localDB.isFavorite(firebaseRecyclerAdapter.getRef(position).getKey()))
                        {
                            localDB.addToFavorites(firebaseRecyclerAdapter.getRef(position).getKey());
                            viewHolder.favoriteIcon.setImageResource(R.drawable.ic_favorite_black_24dp);
                            Toast.makeText(getBaseContext(),""+model.getName()+" is added to Favorites",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            localDB.removeFromFavorites(firebaseRecyclerAdapter.getRef(position).getKey());
                            viewHolder.favoriteIcon.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                            Toast.makeText(getBaseContext(),""+model.getName()+" is removed from Favorites",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                final Product product = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent intent = new Intent(ProductList.this,ProductDetail.class);
                        intent.putExtra("FoodId",firebaseRecyclerAdapter.getRef(position).getKey());
                        startActivity(intent);
                    }
                });

            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }


}
