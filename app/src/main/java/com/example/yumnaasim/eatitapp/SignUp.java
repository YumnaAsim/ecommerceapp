package com.example.yumnaasim.eatitapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import Common.Common;
import model.User;

public class SignUp extends AppCompatActivity {
    Button btnSignUp;
    EditText editPhone, editPassword, editText, editSecure;
    FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Sign Up");
        setContentView(R.layout.activity_sign_up);
        firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference table = firebaseDatabase.getReference("User");
        editText = (MaterialEditText) findViewById(R.id.username);
        editPhone = (MaterialEditText) findViewById(R.id.phone);
        editSecure = (MaterialEditText) findViewById(R.id.secureCode);
        editPassword = (MaterialEditText) findViewById(R.id.password);
        btnSignUp = (Button) findViewById(R.id.sign_up);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editPhone.getText().toString().equals("")
                        && editPassword.getText().toString().equals("")
                        && editSecure.getText().toString().equals("")
                        && editText.getText().toString().equals("")) {
                    Toast.makeText(getBaseContext(),
                            "Invalid Input", Toast.LENGTH_SHORT).show();
                } else {
                    if (Common.isConnectedToInternet(getBaseContext())) {
                        final ProgressDialog progressDialog = new ProgressDialog(SignUp.this);
                        progressDialog.setMessage("Please wait ..");
                        progressDialog.show();
                        table.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.child(editPhone.getText().toString()).exists()) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Phone Number is already registered", Toast.LENGTH_LONG).show();
                                } else {
                                    progressDialog.dismiss();
                                    User user = new User(editText.getText().toString(),
                                            editPassword.getText().toString(),
                                            editSecure.getText().toString());
                                    table.child(editPhone.getText().toString()).setValue(user);
                                    Toast.makeText(getApplicationContext(), "Sign Up Completed!", Toast.LENGTH_LONG).show();
                                    startActivity(new Intent(SignUp.this, MainActivity.class));

                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    } else {
                        Toast.makeText(getApplicationContext(), "Please check your connection!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
        });
    }

}
