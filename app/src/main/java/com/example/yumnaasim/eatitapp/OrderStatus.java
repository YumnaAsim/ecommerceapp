package com.example.yumnaasim.eatitapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import Common.Common;
import ViewHolder.OrderViewHolder;
import model.Order;
import model.Request;


public class OrderStatus extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference requests;
    FirebaseRecyclerAdapter<Request, OrderViewHolder> firebaseRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Orders");
        setContentView(R.layout.activity_order_status);
        recyclerView = (RecyclerView) findViewById(R.id.list_orders);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        database = FirebaseDatabase.getInstance();
        requests = database.getReference("Requests");
        
        loadOrders(Common.currentUser.getPhoneNumber());
    }

    private void loadOrders(String phoneNumber) {
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Request, OrderViewHolder>(
                Request.class,
                R.layout.order_layout,
                OrderViewHolder.class,
                requests.orderByChild("phoneNumber")
                    .equalTo(phoneNumber)
        ) {
            @Override
            protected void populateViewHolder(OrderViewHolder viewHolder, Request model, int position) {
                viewHolder.orderID.setText(firebaseRecyclerAdapter.getRef(position).getKey());
                viewHolder.orderPhone.setText(model.getPhoneNumber());
                viewHolder.orderAddress.setText(model.getAddress());
                viewHolder.orderStatus.setText(convertToStatus(model.getStatus()));
            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    private String convertToStatus(int status) {
        if (status == 0)
            return "Placed";
        else if (status == 1)
            return "On my way";
        else
            return "Shipped";
    }
}
