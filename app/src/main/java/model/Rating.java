package model;

/**
 * Created by YumnaAsim on 9/28/2017.
 */
public class Rating {
    String phoneNumber;
    String foodID;
    String ratingValue;
    String comment;

    public Rating() {
    }

    public Rating(String phoneNumber, String foodID, String ratingValue, String comment) {
        this.phoneNumber = phoneNumber;
        this.foodID = foodID;
        this.ratingValue = ratingValue;
        this.comment = comment;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFoodID() {
        return foodID;
    }

    public void setFoodID(String foodID) {
        this.foodID = foodID;
    }

    public String getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(String ratingValue) {
        this.ratingValue = ratingValue;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
