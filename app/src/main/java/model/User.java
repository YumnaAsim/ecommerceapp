package model;

/**
 * Created by YumnaAsim on 8/29/2017.
 */
public class User {

    private String username;
    private String password;
    private String phoneNumber;
    private String secureCode;

    public User() {
    }

    public User(String username, String password,String secureCode) {
        this.username = username;
        this.password = password;
        this.secureCode = secureCode;
    }

    public String getSecureCode() {
        return secureCode;
    }

    public void setSecureCode(String secureCode) {
        this.secureCode = secureCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
