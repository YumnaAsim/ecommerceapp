package model;

import java.util.List;

/**
 * Created by YumnaAsim on 9/14/2017.
 */
public class Request {
    private String name;
    private String address;
    private String phoneNumber;
    private String total;
    private List<Order> products;
    private int status;

    public Request() {
    }

    public Request(String name, String address, String phoneNumber, String total, List<Order> products) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.total = total;
        this.products = products;
        this.status = 0; // 0 default , 0: placed, 1: shipping, 2: shipped
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<Order> getProducts() {
        return products;
    }

    public void setProducts(List<Order> products) {
        this.products = products;
    }
}
