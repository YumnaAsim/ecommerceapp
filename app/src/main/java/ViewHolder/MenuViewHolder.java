package ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yumnaasim.eatitapp.R;

import Interface.ItemClickListener;

/**
 * Created by YumnaAsim on 8/31/2017.
 */
public class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView menuName;
    public ImageView menuImage;
    ItemClickListener itemClickListener;

    public MenuViewHolder(View itemView) {
        super(itemView);
        menuName = (TextView) itemView.findViewById(R.id.menu_name);
        menuImage = (ImageView) itemView.findViewById(R.id.menu_image);
        itemView.setOnClickListener(this);
    }
    public void setItemClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }



    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);

    }
}
