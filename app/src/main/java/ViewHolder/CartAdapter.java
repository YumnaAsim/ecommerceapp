package ViewHolder;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.example.yumnaasim.eatitapp.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import Common.Common;
import model.Order;

/**
 * Created by YumnaAsim on 9/13/2017.
 */
class CartViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener
{
    TextView cartName,priceTxt;
    ImageView cartCountImg;

    public void setCartName(TextView cartName) {
        this.cartName = cartName;
    }

    /*The elements made in layout file for a single view of recycler view,
    * these all elements are initialized in it's constructor*/

    public CartViewHolder(View itemView) {
        super(itemView);
        cartName = (TextView) itemView.findViewById(R.id.cart_item_name);
        priceTxt = (TextView) itemView.findViewById(R.id.cart_item_price);
        cartCountImg = (ImageView) itemView.findViewById(R.id.cart_item_count);
        itemView.setOnCreateContextMenuListener(this);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("Select action:");
        menu.add(0,0,getAdapterPosition(),Common.DELETE);
    }
}
public class CartAdapter extends RecyclerView.Adapter<CartViewHolder> {

    private List<Order> listData = new ArrayList<>();
    private Context context;

    public CartAdapter(List<Order> listData, Context context) {
        this.listData = listData;
        this.context = context;
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.cart_layout,parent,false);
        return new CartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CartViewHolder holder, int position) {
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(""+listData.get(position).getQuantity(), Color.RED);
        holder.cartCountImg.setImageDrawable(drawable);

        Locale locale = new Locale("en","US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        int price = (Integer.parseInt(listData.get(position).getPrice())) *
                (Integer.parseInt(listData.get(position).getQuantity()));
        holder.priceTxt.setText(fmt.format(price));
        holder.cartName.setText(listData.get(position).getProductName());

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }


}
