package ViewHolder;

/**
 * Created by YumnaAsim on 9/1/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yumnaasim.eatitapp.R;

import Interface.ItemClickListener;


/**
 * Created by YumnaAsim on 8/31/2017.
 */
public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView productName;
    public ImageView productImage, favoriteIcon;
    ItemClickListener itemClickListener;

    public ProductViewHolder(View itemView) {
        super(itemView);
        productName = (TextView) itemView.findViewById(R.id.product_name);
        productImage = (ImageView) itemView.findViewById(R.id.product_image);
        favoriteIcon = (ImageView) itemView.findViewById(R.id.favIcon);
        itemView.setOnClickListener(this);
    }
    public void setItemClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }



    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);

    }
}

