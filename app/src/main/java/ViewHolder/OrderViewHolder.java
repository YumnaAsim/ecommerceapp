package ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.yumnaasim.eatitapp.R;

import Interface.ItemClickListener;

/**
 * Created by YumnaAsim on 9/14/2017.
 */
public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView orderID,orderStatus,orderPhone,orderAddress;
//    private ItemClickListener itemClickListener;

    public OrderViewHolder(View itemView) {
        super(itemView);
        orderID = (TextView) itemView.findViewById(R.id.order_id);
        orderStatus = (TextView) itemView.findViewById(R.id.order_status);
        orderPhone = (TextView) itemView.findViewById(R.id.order_phone);
        orderAddress = (TextView) itemView.findViewById(R.id.order_address);
        itemView.setOnClickListener(this);
    }

//    public void setItemClickListener(ItemClickListener itemClickListener) {
//        this.itemClickListener = itemClickListener;
//    }

    @Override
    public void onClick(View v) {
//        itemClickListener.onClick(v,getAdapterPosition(),false);
    }
}
